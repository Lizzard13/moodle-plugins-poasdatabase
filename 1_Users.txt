﻿DROP TABLE IF EXISTS error_users;
DROP table IF EXISTS error_group;
DROP table IF EXISTS error_rating;
DROP table IF EXISTS users_result_table;
DROP table IF EXISTS recr_tmp;
DROP table IF EXISTS dec_tmp;
DROP table IF EXISTS facult_tmp;

SET SQL_SAFE_UPDATES = 0;
UPDATE syllabi_raw_table
SET syllabus_edu_type_code = 000000012, syllabus_edu_type_name = "Среднее профессиональное"
WHERE syllabus_faculty_quick_entry_number = 302;
SET SQL_SAFE_UPDATES = 1;


#Создать таблицу
CREATE TABLE `error_users` (
  `user_name` VARCHAR(255) NULL,
  `user_surname` VARCHAR(255) NULL,
  `user_patronymic` VARCHAR(255) NULL,
  `user_birth_date` VARCHAR(255) NULL,
  `user_code` VARCHAR(255) NULL,
  `error_type` VARCHAR(255) NULL
  );
  
  
  
#Найти преподавателей без логинов 
insert into error_users (user_code, user_surname, user_name, user_patronymic, user_birth_date, error_type)
select staff_code as user_code, staff_surname as user_surname, staff_name as user_name, 
staff_patronymic as user_patronymic, staff_birth_date as staff_birth_date, "нет логинов" as error_type
from staff_raw_table
where staff_login = "" or staff_password = "" or staff_email = "";

#Убрать из основной таблицы
SET SQL_SAFE_UPDATES = 0;
delete from staff_raw_table 
where staff_login = "" or staff_password = "" or staff_email = "";
SET SQL_SAFE_UPDATES = 1;

#Найти учеников без логинов 
insert into error_users (user_code, user_surname, user_name, user_patronymic, user_birth_date, error_type)
select student_code as user_code, student_surname as user_surname, student_name as user_name, 
student_patronymic as user_patronymic, student_birth_date as user_birth_date, "нет логинов" as error_type
from students_raw_table
where student_login = "" or student_password = "" or student_email = "";

#Убрать из основной таблицы
SET SQL_SAFE_UPDATES = 0;
delete from students_raw_table 
where student_login = "" or student_password = "" or student_email = "";
SET SQL_SAFE_UPDATES = 1;

#Найти учеников-дубликатов
insert into error_users (user_code, user_surname, user_name, user_patronymic, user_birth_date, error_type)
Select t1.student_code as user_code, t1.student_surname as user_surname, t1.student_name as user_name, 
t1.student_patronymic as user_patronymic, t1.student_birth_date as user_birth_date, "несколько логинов" as error_type from students_raw_table as t1
inner join students_raw_table as t2
on t1.student_surname = t2.student_surname
and t1.student_name = t2.student_name
and t1.student_patronymic = t2.student_patronymic
and t1.student_birth_date = t2.student_birth_date
where not (t1.student_login = t2.student_login);

#Удалить
SET SQL_SAFE_UPDATES = 0;
delete from students_raw_table 
where student_code in (Select distinct user_code from error_users);
SET SQL_SAFE_UPDATES = 1;

#Найти предодавателей-дубликатов
insert into error_users (user_code, user_surname, user_name, user_patronymic, user_birth_date, error_type)
Select t1.staff_code as user_code, t1.staff_surname as user_surname, t1.staff_name as user_name, 
t1.staff_patronymic as user_patronymic, t1.staff_birth_date as user_birth_date, "несколько логинов" as error_type from staff_raw_table as t1
inner join staff_raw_table as t2
on t1.staff_surname = t2.staff_surname
and t1.staff_name = t2.staff_name
and t1.staff_patronymic = t2.staff_patronymic
and t1.staff_birth_date = t2.staff_birth_date
where not (t1.staff_login = t2.staff_login);

#Удалить
SET SQL_SAFE_UPDATES = 0;
delete from staff_raw_table 
where staff_code in (Select distinct user_code from error_users);
SET SQL_SAFE_UPDATES = 1;

#Найти группы с двумя учебными планами
create table error_group
select t1.group_code, t1.group_name, t1.group_faculty_quick_entry_number, t1.group_syllabus, "несколько учебных планов" as error_type
from groups_raw_table as t1
inner join groups_raw_table as t2
on t1.group_code = t2.group_code
where not (t1.group_syllabus = t2.group_syllabus);

#Найти группы без состава
insert into error_group(group_code, group_name, group_faculty_quick_entry_number, group_syllabus,error_type)
select distinct group_code, group_name, group_faculty_quick_entry_number, group_syllabus, "нет состава группы" from groups_raw_table where group_code not in (
select distinct group_code from groups_raw_table inner join group_composition_raw_table
on group_composition_raw_table.group_composition_group_code = groups_raw_table.group_code
);

#Найти группы вне расписания
insert into error_group(group_code, group_name, group_faculty_quick_entry_number, group_syllabus,error_type)
select distinct group_code, group_name, group_faculty_quick_entry_number, group_syllabus, "нет в расписании" from groups_raw_table where group_code not in (
select distinct rating_group_code from rating_raw_table
);


#Функция выделения курса из названия группы
DROP FUNCTION IF EXISTS getCource;
DELIMITER $$
CREATE FUNCTION getCource (s VARCHAR(255)) 
RETURNS int
DETERMINISTIC
BEGIN 
  DECLARE res VARCHAR(255);
  DECLARE isSpaceBefore bool;
  declare i int;
  set isSpaceBefore = false;
  set i = 1;
  set res = "";
  while i < length(s) do
	if SUBSTRING(s, i, 1) = "0" or SUBSTRING(s, i, 1) = "1" or SUBSTRING(s, i, 1) = "2" or SUBSTRING(s, i, 1) = "3"
     or SUBSTRING(s, i, 1) = "4" or SUBSTRING(s, i, 1) = "5" or SUBSTRING(s, i, 1) = "6" or SUBSTRING(s, i, 1) = "7"
      or SUBSTRING(s, i, 1) = "8" or SUBSTRING(s, i, 1) = "9" then
		set res = CONCAT(res, SUBSTRING(s, i, 1));
        RETURN res;
	end if;
	set i = i + 1;
  end while;

  RETURN res;
END$$
DELIMITER ;

ALTER TABLE `error_group` 
CHANGE COLUMN `error_type` `error_type` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT '' ;

#планы с неверным курсом - стандартные названия
insert into error_group(group_code, group_name, group_faculty_quick_entry_number, group_syllabus,error_type)
SELECT group_code, group_name, group_faculty_quick_entry_number, group_syllabus, "несоответствие курса группы курсу плана" FROM groups_raw_table
inner join syllabi_raw_table
on group_syllabus = syllabus_code
WHERE not syllabus_course = getCource (group_name)
and group_faculty_quick_entry_number not in (108, 109, 110, 300, 301, 302, 114)
;


SET SQL_SAFE_UPDATES = 0;
DELETE from error_group WHERE group_faculty_quick_entry_number = 114 and error_type = "несоответствие курса группы курсу плана";
SET SQL_SAFE_UPDATES = 1;






#Выделить год для 108-110 (две последние цифры)
DROP FUNCTION IF EXISTS getCource108110;
DELIMITER $$
CREATE FUNCTION getCource108110 (s VARCHAR(255)) 
RETURNS int
DETERMINISTIC
BEGIN 
  DECLARE res VARCHAR(255);
  declare i int;
  set i = 1;
  set res = "";
  while i < length(s) do
	if SUBSTRING(reverse(s), i, 1) = "-" then
		set res = CONCAT(res, SUBSTRING(reverse(s), i - 2, 2));
        RETURN reverse(res);
	end if;
	set i = i + 1;
  end while;

  RETURN res;
END$$
DELIMITER ;

#планы с неверным курсом для 108-110
insert into error_group(group_code, group_name, group_faculty_quick_entry_number, group_syllabus,error_type)
SELECT group_code, group_name, group_faculty_quick_entry_number, group_syllabus, "несоответствие курса группы курсу плана"
FROM groups_raw_table
inner join syllabi_raw_table
on group_syllabus = syllabus_code
WHERE (not syllabus_course = 21 - getCource108110 (group_name))
and group_faculty_quick_entry_number in (108, 109, 110)
;


#Функция получения учебного курса из групп 300-302
DROP FUNCTION IF EXISTS getCourceYear1;
DELIMITER $$
CREATE FUNCTION getCourceYear1 (s VARCHAR(255)) 
RETURNS int
DETERMINISTIC
BEGIN 
  DECLARE res VARCHAR(255);
  DECLARE isSpaceBefore bool;
  declare i int;
  set isSpaceBefore = false;
  set i = 1;
  set res = "";
  while i < length(s) do
	if SUBSTRING(s, i, 1) = "-" then
		set res = CONCAT(res, SUBSTRING(s, i + 1, 2));
        RETURN res;
	end if;
	set i = i + 1;
  end while;

  RETURN res;
END$$
DELIMITER ;

#планы с неверным курсом для 300-302
insert into error_group(group_code, group_name, group_faculty_quick_entry_number, group_syllabus,error_type)
SELECT group_code, group_name, group_faculty_quick_entry_number, group_syllabus, "несоответствие курса группы курсу плана"
FROM groups_raw_table
inner join syllabi_raw_table
on group_syllabus = syllabus_code
WHERE (not syllabus_course = 21 - getCourceYear1(group_name))
and group_faculty_quick_entry_number in (300, 301, 302)
;

#Удаление неверных групп
SET SQL_SAFE_UPDATES = 0;
delete from groups_raw_table
WHERE groups_raw_table.group_code in (select group_code from error_group);
SET SQL_SAFE_UPDATES = 1;

#Найти курсы рейтинга без преподавателя
create table error_rating
SELECT rating_raw_table.*, "нет преподавателя" as error_type FROM rating_raw_table
where rating_teacher_login = "";

#Не существует соответствующей курсу группы
insert into error_rating
SELECT rating_raw_table.*, "нет такой группы" as error_type FROM rating_raw_table
where rating_group_code not in(
	select distinct group_code from groups_raw_table
);

#Не приписана кафедра
insert into error_rating
SELECT rating_raw_table.*, "нет кафедры" as error_type FROM rating_raw_table
where rating_department_code = "";

#Создание результирующей таблицы пользователей

create table users_result_table
SELECT student_code as users_result_id, student_name as users_result_name, student_surname as users_result_surname, student_patronymic as users_result_patronymic, 
student_login as users_result_login, md5(student_password) as users_result_md5password, student_email as users_result_email, null as users_result_department,
null as users_result_city, "русский" as users_result_lang, "Волгоградский Государственный Технический Университет" as users_result_institution FROM students_raw_table;

#Выделение управления в отдельные таблицы

create table recr_tmp 
SELECT * FROM staff_raw_table
WHERE staff_department = "Ректорат";


create table dec_tmp 
SELECT * FROM staff_raw_table
WHERE staff_department like "Деканат%";


create table facult_tmp 
SELECT * FROM staff_raw_table
WHERE staff_department like "Факультет%";

SET SQL_SAFE_UPDATES = 0;
Delete FROM staff_raw_table
WHERE staff_department = "Ректорат";

Delete FROM staff_raw_table
WHERE staff_department like "Деканат%";

Delete FROM staff_raw_table
WHERE staff_department like "Факультет%";
SET SQL_SAFE_UPDATES = 1;


SET SQL_SAFE_UPDATES = 0;
delete from users_result_table
where isnull(users_result_email);
SET SQL_SAFE_UPDATES = 1;


ALTER TABLE `users_result_table` 
CHANGE COLUMN `users_result_id` `users_result_id` VARCHAR(255) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_name` `users_result_name` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_surname` `users_result_surname` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_patronymic` `users_result_patronymic` VARCHAR(255) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_login` `users_result_login` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_md5password` `users_result_md5password` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_email` `users_result_email` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_department` `users_result_department` VARCHAR(255) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_city` `users_result_city` VARCHAR(255) NULL DEFAULT NULL ,
CHANGE COLUMN `users_result_lang` `users_result_lang` VARCHAR(30) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT '' ,
CHANGE COLUMN `users_result_institution` `users_result_institution` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT '' ;

ALTER TABLE `users_result_table` 
CHANGE COLUMN `users_result_lang` `users_result_lang` VARCHAR(30) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT 'русский' ,
CHANGE COLUMN `users_result_institution` `users_result_institution` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT 'Волгоградский Государственный Технический Университет' ;


#Добавить всех преподавателей с высшим из рангов
INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select staff_department, staff_code, staff_name, staff_surname, staff_patronymic, staff_login, md5(staff_password), staff_email
from staff_raw_table 
where staff_login not in (select staff_login from dec_tmp) 
and staff_login not in (select staff_login from recr_tmp) 
and staff_login not in (select staff_login from facult_tmp)
and staff_login not in (select users_result_login from users_result_table)
);


INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select staff_department, staff_code, staff_name, staff_surname, staff_patronymic, staff_login, md5(staff_password), staff_email
from facult_tmp 
where staff_login not in (select staff_login from dec_tmp) 
and staff_login not in (select staff_login from recr_tmp) 
and staff_login not in (select users_result_login from users_result_table)
);

INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select staff_department, staff_code, staff_name, staff_surname, staff_patronymic, staff_login, md5(staff_password), staff_email 
from dec_tmp 
where staff_login not in (select staff_login from recr_tmp) 
and staff_login not in (select users_result_login from users_result_table)
);

INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select staff_department, staff_code, staff_name, staff_surname, staff_patronymic, staff_login, md5(staff_password) , staff_email 
from recr_tmp 
where staff_login not in (select users_result_login from users_result_table)
);

INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select faculty_fullname, faculty_coordinator_code, faculty_eos_coordinator_name, faculty_eos_coordinator_surname, faculty_eos_coordinator_patronymic, faculty_coordinator_login, md5(faculty_coordinator_password), faculty_eos_coordinator_email
from faculties_raw_table 
where faculty_coordinator_login not in (select users_result_login from users_result_table)
);


INSERT INTO users_result_table (users_result_department, users_result_id, users_result_name, users_result_surname, users_result_patronymic, users_result_login, users_result_md5password, users_result_email)
(select department_fullname, department_coordinator_code,  department_eos_coordinator_name, department_eos_coordinator_surname, department_eos_coordinator_patronymic, department_coordinator_login, md5(department_coordinator_password), department_eos_coordinator_email
from departments_raw_table 
where department_coordinator_login not in (select users_result_login from users_result_table)
);



#Найти в рейтинге строки, в которых преподаватель не имеет записей в таблице пользователей
insert into error_rating
SELECT rating_raw_table.*, "нет пользователя" as error_type FROM rating_raw_table
where rating_teacher_login not in (
	select users_result_login from users_result_table
);

#Удалить ошибки из рейтинга
SET SQL_SAFE_UPDATES = 0;
delete from rating_raw_table
where (rating_group_code, rating_group_name, rating_discipline_code, rating_discipline_fullname, rating_teacher_code, rating_teacher_surname, rating_teacher_name,
rating_teacher_patronymic, rating_teacher_login, rating_teacher_site_id, rating_department_code, rating_department_shortname, rating_department_quick_entry_number, 
rating_department_eos_id, rating_department_eos_coordinator_code, rating_department_coordinator_surname, rating_department_coordinator_name,
rating_department_coordinator_patronymic, rating_department_coordinator_login, rating_department_coordinator_site_id, rating_faculty_quick_entry_number)
IN(SELECT rating_group_code, rating_group_name, rating_discipline_code, rating_discipline_fullname, rating_teacher_code, rating_teacher_surname, rating_teacher_name,
rating_teacher_patronymic, rating_teacher_login, rating_teacher_site_id, rating_department_code, rating_department_shortname, rating_department_quick_entry_number, 
rating_department_eos_id, rating_department_eos_coordinator_code, rating_department_coordinator_surname, rating_department_coordinator_name,
rating_department_coordinator_patronymic, rating_department_coordinator_login, rating_department_coordinator_site_id, rating_faculty_quick_entry_number 
from error_rating);
SET SQL_SAFE_UPDATES = 1;
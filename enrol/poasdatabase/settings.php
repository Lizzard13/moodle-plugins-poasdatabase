<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Database enrolment plugin settings and presets.
 *
 * @package    enrol_poasdatabase
 * @copyright  2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('enrol_poasdatabase_settings', '', get_string('pluginname_desc', 'enrol_poasdatabase')));

    $settings->add(new admin_setting_heading('enrol_poasdatabase_exdbheader', get_string('settingsheaderdb', 'enrol_poasdatabase'), ''));

    $options = array('', "access", "ado_access", "ado", "ado_mssql", "borland_ibase", "csv", "db2", "fbsql", "firebird", "ibase", "informix72", "informix", "mssql", "mssql_n", "mssqlnative", "mysql", "mysqli", "mysqlt", "oci805", "oci8", "oci8po", "odbc", "odbc_mssql", "odbc_oracle", "oracle", "pdo", "postgres64", "postgres7", "postgres", "proxy", "sqlanywhere", "sybase", "vfp");
    $options = array_combine($options, $options);
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/dbtype', get_string('dbtype', 'enrol_poasdatabase'), get_string('dbtype_desc', 'enrol_poasdatabase'), '', $options));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbhost', get_string('dbhost', 'enrol_poasdatabase'), get_string('dbhost_desc', 'enrol_poasdatabase'), 'localhost'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbuser', get_string('dbuser', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configpasswordunmask('enrol_poasdatabase/dbpass', get_string('dbpass', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbname', get_string('dbname', 'enrol_poasdatabase'), get_string('dbname_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbencoding', get_string('dbencoding', 'enrol_poasdatabase'), '', 'utf-8'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/dbsetupsql', get_string('dbsetupsql', 'enrol_poasdatabase'), get_string('dbsetupsql_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/dbsybasequoting', get_string('dbsybasequoting', 'enrol_poasdatabase'), get_string('dbsybasequoting_desc', 'enrol_poasdatabase'), 0));

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/debugdb', get_string('debugdb', 'enrol_poasdatabase'), get_string('debugdb_desc', 'enrol_poasdatabase'), 0));



    $settings->add(new admin_setting_heading('enrol_poasdatabase_localheader', get_string('settingsheaderlocal', 'enrol_poasdatabase'), ''));

    $options = array('id'=>'id', 'idnumber'=>'idnumber', 'shortname'=>'shortname');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localcoursefield', get_string('localcoursefield', 'enrol_poasdatabase'), '', 'idnumber', $options));

    $options = array('id'=>'id', 'idnumber'=>'idnumber', 'email'=>'email', 'username'=>'username'); // only local users if username selected, no mnet users!
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localuserfield', get_string('localuserfield', 'enrol_poasdatabase'), '', 'idnumber', $options));

    $options = array('id'=>'id', 'shortname'=>'shortname');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localrolefield', get_string('localrolefield', 'enrol_poasdatabase'), '', 'shortname', $options));

    $options = array('id'=>'id', 'idnumber'=>'idnumber');
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/localcategoryfield', get_string('localcategoryfield', 'enrol_poasdatabase'), '', 'id', $options));


    $settings->add(new admin_setting_heading('enrol_poasdatabase_remoteheader', get_string('settingsheaderremote', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteenroltable', get_string('remoteenroltable', 'enrol_poasdatabase'), get_string('remoteenroltable_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remotecoursefield', get_string('remotecoursefield', 'enrol_poasdatabase'), get_string('remotecoursefield_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteuserfield', get_string('remoteuserfield', 'enrol_poasdatabase'), get_string('remoteuserfield_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoterolefield', get_string('remoterolefield', 'enrol_poasdatabase'), get_string('remoterolefield_desc', 'enrol_poasdatabase'), ''));

    $otheruserfieldlabel = get_string('remoteotheruserfield', 'enrol_poasdatabase');
    $otheruserfielddesc  = get_string('remoteotheruserfield_desc', 'enrol_poasdatabase');
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/remoteotheruserfield', $otheruserfieldlabel, $otheruserfielddesc, ''));

    if (!during_initial_install()) {
        $options = get_default_enrol_roles(context_system::instance());
        $student = get_archetype_roles('student');
        $student = reset($student);
        $settings->add(new admin_setting_configselect('enrol_poasdatabase/defaultrole', get_string('defaultrole', 'enrol_poasdatabase'), get_string('defaultrole_desc', 'enrol_poasdatabase'), $student->id, $options));
    }

    $settings->add(new admin_setting_configcheckbox('enrol_poasdatabase/ignorehiddencourses', get_string('ignorehiddencourses', 'enrol_poasdatabase'), get_string('ignorehiddencourses_desc', 'enrol_poasdatabase'), 0));

    $options = array(ENROL_EXT_REMOVED_UNENROL        => get_string('extremovedunenrol', 'enrol'),
                     ENROL_EXT_REMOVED_KEEP           => get_string('extremovedkeep', 'enrol'),
                     ENROL_EXT_REMOVED_SUSPEND        => get_string('extremovedsuspend', 'enrol'),
                     ENROL_EXT_REMOVED_SUSPENDNOROLES => get_string('extremovedsuspendnoroles', 'enrol'));
    $settings->add(new admin_setting_configselect('enrol_poasdatabase/unenrolaction', get_string('extremovedaction', 'enrol'), get_string('extremovedaction_help', 'enrol'), ENROL_EXT_REMOVED_UNENROL, $options));

////////////////////////////////////////////Lizzard Modifications/////////////////////////////////////////////
    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcategories', 'Создание категорий', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcategoriestable', 'Удалённая таблица категорий', 'Укажите имя таблицы, содержащей перечень категорий, которые должны быть автоматически созданы. При пустом значении категории не создаются.',''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcategoryname', 'Поле полного имени новой категории', '', 'categoryname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcategoryidnumber', 'Поле номера ID новой категории', '', 'idnumber'));


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////Lizzard Modifications/////////////////////////////////////////////
    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcohortsheader', 'Создание глобальных групп', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohorttable', 'Удалённая таблица глобальных групп', 'Укажите имя таблицы, содержащей перечень когорт, которые должны быть автоматически созданы. При пустом значении когорты не создаются.',''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortname', 'Поле полного имени новой глобальной группы', '', 'cohortname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortdescription', 'Поле определения новой глобальной группы', '', 'description'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcohortidnumber', 'Поле номера ID новой глобальной группы', '', 'idnumber'));


    $settings->add(new admin_setting_heading('enrol_poasdatabase_usercohortsheader', 'Распределение глобальных групп', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/usercohorttable', 'Удалённая таблица', 'Укажите имя таблицы, содержащей перечень связей пользователей и когорт, которые должны быть автоматически созданы. При пустом значении связи не создаются.',''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/cohortidnumber', 'Поле idnumber глобальной группы в таблице', '', 'cohortidnumber'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/userlogin', 'Поле пользователя', '', 'userlogin'));

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $settings->add(new admin_setting_heading('enrol_poasdatabase_newcoursesheader', get_string('settingsheadernewcourses', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursetable', get_string('newcoursetable', 'enrol_poasdatabase'), get_string('newcoursetable_desc', 'enrol_poasdatabase'), ''));


/////////////////////////////////////////////Lizzard Modifications/////////////////////////////////////////
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/tagstable', 'Таблица тегов', '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/sqlidnumberfield', 'Поле idnumber курса в таблице тегов', '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/sqltagnamefield', 'Поле тега в таблице тегов', '', ''));
/////////////////////////////////////////////////////////////////////////////////////////////////////////


    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursefullname', get_string('newcoursefullname', 'enrol_poasdatabase'), '', 'fullname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcourseshortname', get_string('newcourseshortname', 'enrol_poasdatabase'), '', 'shortname'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcourseidnumber', get_string('newcourseidnumber', 'enrol_poasdatabase'), '', 'idnumber'));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/newcoursecategory', get_string('newcoursecategory', 'enrol_poasdatabase'), '', ''));

    $settings->add(new admin_settings_coursecat_select('enrol_poasdatabase/defaultcategory',
        get_string('defaultcategory', 'enrol_poasdatabase'),
        get_string('defaultcategory_desc', 'enrol_poasdatabase'), 1));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/templatecourse', get_string('templatecourse', 'enrol_poasdatabase'), get_string('templatecourse_desc', 'enrol_poasdatabase'), ''));

    $settings->add(new admin_setting_heading('enrol_poasdatabase_coursescohortsheader', 'Зачисление на курсы на основании глобальной группы', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupcohortcoursestable', 'Удалённая таблица', 'Укажите имя таблицы, содержащей перечень связей между глобальными группами и курсами для зачисления.',''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupcohortidnumber', 'Поле idnumber глобальной группы в таблице', '', ''));
    $settings->add(new admin_setting_configtext('enrol_poasdatabase/courseidnumber', 'Поле idnumber курса в таблице', '', ''));

    $settings->add(new admin_setting_configtext('enrol_poasdatabase/groupname', 'Поле наименования группы', '', ''));
}

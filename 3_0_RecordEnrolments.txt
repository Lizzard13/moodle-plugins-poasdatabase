CREATE TABLE IF NOT EXISTS `courses_members_date` (
  `course_idnumber` VARCHAR(255) NOT NULL,
  `member_login` VARCHAR(255) NOT NULL,
  `member_role` VARCHAR(255) NOT NULL,
  `record_date` DATE NOT NULL,
  PRIMARY KEY (`course_idnumber`, `member_login`, `member_role`, `record_date`));

insert into courses_members_date (course_idnumber, member_login, member_role, record_date)
select distinct course_idnumber, member_login, member_role, CURDATE() from courses_members
;